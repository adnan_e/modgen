import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import org.omg.CORBA.Environment;

import javax.lang.model.element.Modifier;
import java.io.*;
import java.util.HashMap;

public class Main {
    private static HashMap<String, String> options = new HashMap<String, String>();
    private static String[] required = new String[] {
            "hasRepo",
            "name",
            "package",
            "outDir"
    };

    static {
        options.put("hasRepo", "true");
        options.put("hasController", "true");
    }

    private static void die(String why) {
        System.err.println(why);
        System.exit(1);
    }

    private static boolean isRequired(String s) {
        for ( String str : required ) {
            if ( str.equals(str) ) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        for ( int i = 0; i < args.length - 1; i += 2 ) {
            String s = args[i];

            if ( !options.containsKey(s) && !isRequired(s) ) {
                die("Invalid arg " + s);
                return;
            }
            options.put(s, args[i + 1]);
        }

        for ( String s : required ) {
            if ( !options.containsKey(s) ) {
                die("Missing arg " + s);
                return;
            }
        }


        spawnModule(options);
    }


    private static void spawnModule(HashMap<String, String> options) {
        String cpackage = options.get("package");
        String serviceName = options.get("name");
        File outDir = new File(options.get("outDir"));
        boolean hasRepo = options.get("hasRepo").equalsIgnoreCase("true");

        cpackage += "." + serviceName.toLowerCase();

        TypeSpec ebService = Generators.generateEbService(cpackage, serviceName);
        TypeSpec ebServiceImpl = Generators.generateEbServiceImpl(cpackage, serviceName);
        TypeSpec ebServiceVerticle = Generators.generateVerticle(cpackage, serviceName);
        TypeSpec dataProvider = Generators.generateDataProvider(cpackage, serviceName);
        TypeSpec repository = Generators.generateRepository(cpackage, serviceName);
        TypeSpec service = Generators.generateService(cpackage, serviceName);
        TypeSpec serviceEbProxy = Generators.generateServiceEbProxy(cpackage, serviceName);
        TypeSpec serviceImpl = Generators.generateServiceImpl(cpackage, serviceName);

        JavaFile ebServiceFile = JavaFile.builder(cpackage, ebService).build();
        JavaFile ebServiceImplFile = JavaFile.builder(cpackage, ebServiceImpl).build();
        JavaFile ebServiceVerticleFile = JavaFile.builder(cpackage, ebServiceVerticle).build();
        JavaFile dataProviderFile = JavaFile.builder(cpackage, dataProvider).build();
        JavaFile repositoryFile = JavaFile.builder(cpackage, repository).build();
        JavaFile serviceFile = JavaFile.builder(cpackage, service).build();
        JavaFile serviceEbProxyFile = JavaFile.builder(cpackage, serviceEbProxy).build();
        JavaFile serviceImplFile = JavaFile.builder(cpackage, serviceImpl).build();

        // The package-info.java has to be manually generated since it's format isn't supported by Java Poet
        String packageInfo = String.format(
                "@ModuleGen(" +
                        "name = \"%s\", " +
                        "groupPackage = \"%s\")\n" +
                "package com.busfive.citypass.api.graphql.services.%s;\n" +
                "\n" +
                "import io.vertx.codegen.annotations.ModuleGen;",
                cpackage.replace('.', '-'),
                cpackage,
                serviceName.toLowerCase());


        try {
            System.out.println("Writing files to " + outDir.getAbsolutePath());

            outDir.mkdirs();

            ebServiceFile.writeTo(outDir);
            ebServiceImplFile.writeTo(outDir);
            ebServiceVerticleFile.writeTo(outDir);
            dataProviderFile.writeTo(outDir);
            serviceFile.writeTo(outDir);
            serviceEbProxyFile.writeTo(outDir);
            serviceImplFile.writeTo(outDir);
            if ( hasRepo ) {
                repositoryFile.writeTo(outDir);
            }

            String path = ebServiceFile.packageName.replace('.', '/');

            FileOutputStream stream = new FileOutputStream(
                    new File(outDir.getAbsolutePath() + "/" + path, "package-info.java"));

            stream.write(packageInfo.getBytes());
            stream.flush();
            stream.close();


        } catch ( IOException ex ) {
            ex.printStackTrace();
        }

    }
}
