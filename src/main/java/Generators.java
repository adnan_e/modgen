import com.google.common.reflect.TypeToken;
import com.monri.common.vertx.web.ControllerVerticle;
import com.monri.common.vertx.web.rx.RxOperators;
import com.squareup.javapoet.*;
import io.reactivex.Single;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.impl.AsyncResultSingle;
import io.vertx.reactivex.ext.jdbc.JDBCClient;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;
import java.util.Collections;

public class Generators {
    private static ClassName serviceClass(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("%sService", serviceName));
    }

    private static ClassName ebServiceVertxEbProxy(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("EB%sServiceVertxEBProxy", serviceName));
    }

    private static ClassName ebServiceImplClass(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("EB%sServiceImpl", serviceName));
    }

    private static ClassName ebServiceClass(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("EB%sService", serviceName));
    }

    private static ClassName verticleClass(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("EB%sServiceVerticle", serviceName));
    }

    private static ClassName serviceImpl(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("%sServiceImpl", serviceName));
    }

    private static ClassName repoClass(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("%sRepository", serviceName));
    }

    private static ClassName serviceEbProxyClass(String cpackage, String serviceName) {
         return ClassName.get(cpackage, String.format("%sServiceEBProxy", serviceName));
    }

    private static ClassName dataProviderClass(String cpackage, String serviceName) {
        return ClassName.get(cpackage, String.format("%sDataProvider", serviceName));
    }

    public static TypeSpec generateEbService(String cpackage, String serviceName) {
        String className = String.format("EB%sService", serviceName);

        ClassName serviceClass = serviceClass(cpackage, serviceName);
        ClassName ebServiceVertxEbProxy = ebServiceVertxEbProxy(cpackage, serviceName);
        ClassName ebServiceImplClass = ebServiceImplClass(cpackage, serviceName);

        MethodSpec createMethod = MethodSpec.methodBuilder("create")
                .addModifiers(Modifier.STATIC, Modifier.PUBLIC)
                .returns(ClassName.get(cpackage, className))
                .addParameter(serviceClass, "delegate")
                .addStatement("return new $T(delegate)", ebServiceImplClass)
                .addAnnotation(GenIgnore.class)
                .build();

        MethodSpec createProxyMethod = MethodSpec.methodBuilder("createProxy")
                .addModifiers(Modifier.STATIC, Modifier.PUBLIC)
                .returns(ClassName.get(cpackage, className))
                .addParameter(Vertx.class, "vertx")
                .addStatement("return new $T(vertx, " + className + ".ADDRESS)", ebServiceVertxEbProxy)
                .addAnnotation(GenIgnore.class)
                .build();

        MethodSpec placeHolderMethod = MethodSpec.methodBuilder("placeHolder")
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .addAnnotation(Fluent.class)
                .returns(ClassName.get(cpackage, className))
                .addParameter(new TypeToken<Handler<AsyncResult<JsonObject>>>(){}.getType(), "handler")
                .build();

        FieldSpec addressField = FieldSpec
                .builder(String.class, "ADDRESS")
                .addModifiers(Modifier.STATIC, Modifier.FINAL, Modifier.PUBLIC)
                .initializer(String.format("\"api.service.%s_service\"", serviceName).toLowerCase())
                .build();

        FieldSpec nameField = FieldSpec
                .builder(String.class, "NAME")
                .addModifiers(Modifier.STATIC, Modifier.FINAL, Modifier.PUBLIC)
                .initializer(String.format("\"%s_service\"", serviceName).toLowerCase())
                .build();

        return TypeSpec.interfaceBuilder(className)
                .addAnnotation(ProxyGen.class)
                .addField(addressField)
                .addField(nameField)
                .addMethod(createMethod)
                .addMethod(createProxyMethod)
                .addMethod(placeHolderMethod)
                .build();
    }

    public static TypeSpec generateEbServiceImpl(String cpackage, String serviceName) {
        ClassName ebServiceImplClass = ebServiceImplClass(cpackage, serviceName);
        ClassName serviceClass = serviceClass(cpackage, serviceName);
        ClassName ebServiceClass = ebServiceClass(cpackage, serviceName);


        MethodSpec placeHolderMethod = MethodSpec.methodBuilder("placeHolder")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .returns(ebServiceImplClass)
                .addParameter(new TypeToken<Handler<AsyncResult<JsonObject>>>(){}.getType(), "handler")
                .addStatement("delegate.placeHolder().to($T::toFuture).subscribe(handler::handle)", RxOperators.class)
                .addStatement("return this")
                .build();

        MethodSpec constructor = MethodSpec.constructorBuilder()
                .addParameter(serviceClass, "delegate")
                .addStatement("this.delegate = delegate")
                .build();

        FieldSpec delegateField = FieldSpec
                .builder(serviceClass, "delegate")
                .addModifiers(Modifier.PRIVATE, Modifier.FINAL)
                .build();

        return TypeSpec.classBuilder(ebServiceImplClass)
                .addSuperinterface(ebServiceClass)
                .addField(delegateField)
                .addMethod(constructor)
                .addMethod(placeHolderMethod)
                .build();
    }

    public static TypeSpec generateVerticle(String cpackage, String serviceName) {
        ClassName verticleClass = verticleClass(cpackage, serviceName);
        ClassName serviceClass = serviceClass(cpackage, serviceName);
        ClassName serviceImpl = serviceImpl(cpackage, serviceName);
        ClassName repoClass = repoClass(cpackage, serviceName);
        ClassName ebService = ebServiceClass(cpackage, serviceName);

        MethodSpec startMethod = MethodSpec.methodBuilder("start")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(new TypeToken<Future<Void>>(){}.getType(), "startFuture")
                .addStatement("$T jdbc = $T.createShared(vertx, config().getJsonObject(\"jdbc\"))",
                        JDBCClient.class, JDBCClient.class)
                .addStatement(String.format("final $T %sService = new $T($T.create(jdbc))",
                        serviceName.toLowerCase()), serviceClass, serviceImpl, repoClass)
                .addStatement(String.format("$T.all($T.singletonList(" +
                        "registerAndEventBusService($T.NAME, $T.ADDRESS, $T.class, $T.create(%sService)))).setHandler(" +
                        "this.startHandler(startFuture))", serviceName.toLowerCase()),
                        CompositeFuture.class,
                        Collections.class,
                        ebService,
                        ebService,
                        ebService,
                        ebService)
                .build();

        return TypeSpec.classBuilder(verticleClass)
                .addModifiers(Modifier.PUBLIC)
                .superclass(ControllerVerticle.class)
                .addMethod(startMethod)
                .build();
    }

    public static TypeSpec generateDataProvider(String cpackage, String serviceName) {
        ClassName repoClass = repoClass(cpackage, serviceName);
        ClassName dataProviderClass = dataProviderClass(cpackage, serviceName);

        FieldSpec jdbcClientField = FieldSpec.builder(JDBCClient.class, "jdbcClient")
                .addModifiers(Modifier.PRIVATE)
                .build();

        MethodSpec placeHolderMethod = MethodSpec.methodBuilder("placeHolder")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .returns(new TypeToken<Single<JsonObject>>(){}.getType())
                .addStatement("return $T.just(new $T())", Single.class, JsonObject.class)
                .build();

        MethodSpec constructor = MethodSpec.constructorBuilder()
                .addParameter(JDBCClient.class, "jdbcClient")
                .addStatement("this.jdbcClient = jdbcClient")
                .build();

        return TypeSpec.classBuilder(dataProviderClass)
                .addSuperinterface(repoClass)
                .addField(jdbcClientField)
                .addMethod(constructor)
                .addMethod(placeHolderMethod)
                .build();
    }

    public static TypeSpec generateRepository(String cpackage, String serviceName) {
        ClassName repoClass = repoClass(cpackage, serviceName);
        ClassName dataProviderClass = dataProviderClass(cpackage, serviceName);

        MethodSpec createMethod = MethodSpec.methodBuilder("create")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(JDBCClient.class, "jdbcClient")
                .addStatement("return new $T(jdbcClient)", dataProviderClass)
                .returns(repoClass)
                .build();

        MethodSpec placeHolderMethod = MethodSpec.methodBuilder("placeHolder")
                .returns(new TypeToken<Single<JsonObject>>(){}.getType())
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .build();

        return TypeSpec.interfaceBuilder(repoClass)
                .addMethod(createMethod)
                .addMethod(placeHolderMethod)
                .build();
    }

    public static TypeSpec generateService(String cpackage, String serviceName) {
        ClassName serviceClass = serviceClass(cpackage, serviceName);
        ClassName serviceEbProxyClass = serviceEbProxyClass(cpackage, serviceName);
        ClassName ebServiceClass = ebServiceClass(cpackage, serviceName);


        MethodSpec createMethod = MethodSpec.methodBuilder("createProxy")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(Vertx.class, "vertx")
                .addStatement("return new $T($T.createProxy(vertx))", serviceEbProxyClass, ebServiceClass)
                .returns(serviceClass)
                .build();

        MethodSpec placeHolderMethod = MethodSpec.methodBuilder("placeHolder")
                .returns(new TypeToken<Single<JsonObject>>(){}.getType())
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .build();

        return TypeSpec.interfaceBuilder(serviceClass)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(createMethod)
                .addMethod(placeHolderMethod)
                .build();
    }

    public static TypeSpec generateServiceEbProxy(String cpackage, String serviceName) {
        ClassName serviceEbProxyClass = serviceEbProxyClass(cpackage, serviceName);
        ClassName ebServiceClass = ebServiceClass(cpackage, serviceName);
        ClassName serviceClass = serviceClass(cpackage, serviceName);

        FieldSpec ebServiceField = FieldSpec.builder(ebServiceClass, "delegate")
                .addModifiers(Modifier.PRIVATE, Modifier.FINAL)
                .build();

        MethodSpec constructor = MethodSpec.constructorBuilder()
                .addParameter(ebServiceClass, "delegate")
                .addStatement("this.delegate = delegate")
                .build();

        MethodSpec placeHolderMethod = MethodSpec.methodBuilder("placeHolder")
                .returns(new TypeToken<Single<JsonObject>>(){}.getType())
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .addStatement("return new $T<>(e -> delegate.placeHolder(e::handle))", AsyncResultSingle.class)
                .build();

        return TypeSpec.classBuilder(serviceEbProxyClass)
                .addSuperinterface(serviceClass)
                .addField(ebServiceField)
                .addMethod(constructor)
                .addMethod(placeHolderMethod)
                .build();
    }

    public static TypeSpec generateServiceImpl(String cpackage, String serviceName) {
        ClassName repoClass = repoClass(cpackage, serviceName);
        ClassName serviceClass = serviceClass(cpackage, serviceName);

        FieldSpec repoField = FieldSpec.builder(repoClass, String.format("%sRepository", serviceName.toLowerCase()))
                .addModifiers(Modifier.PRIVATE, Modifier.FINAL)
                .build();

        MethodSpec constructor = MethodSpec.constructorBuilder()
                .addParameter(repoClass, String.format("%sRepository", serviceName.toLowerCase()))
                .addStatement(String.format("this.%sRepository = %sRepository",
                        serviceName.toLowerCase(), serviceName.toLowerCase()))
                .build();

        MethodSpec placeHolderMethod = MethodSpec.methodBuilder("placeHolder")
                .addModifiers(Modifier.PUBLIC)
                .returns(new TypeToken<Single<JsonObject>>(){}.getType())
                .addStatement(String.format("return %sRepository.placeHolder()", serviceName.toLowerCase()))
                .build();

        return TypeSpec.classBuilder(serviceImpl(cpackage, serviceName))
                .addSuperinterface(serviceClass)
                .addField(repoField)
                .addMethod(constructor)
                .addMethod(placeHolderMethod)
                .build();
    }
}
