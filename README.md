# modgen

## Example usage
modgen.jar should be located in the root of the parkmatixApi project
>java -jar modgen.jar name Parkmeter

The above command will create the following file structure:

The repository interface and implementation (set `hasRepo` to false to skip):
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\ParkmeterRepository.java  
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\ParkmeterPostgresDataProvider.java  

The controller (set `hasController` to false to skip)
>\parkmatixApi\api\src\main\java\com\parkmatix\api\v1\controllers\ParkmeterController.java  
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\EBParkmeterService.java  

The service
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\EBParkmeterServiceImpl.java  
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\ParkmeterServiceImpl.java  
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\EBParkmeterServiceVerticle.java  
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\package-info.java  
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\ParkmeterService.java  
>\parkmatixApi\api\src\main\java\com\parkmatix\api\services\parkmeter\ParkmeterServiceEBProxy.java  

## Additional options

### hasRepo
Defaults to `true` when not given.
>java -jar modgen.jar name Parkmeter hasRepo false 

If `false` no repository will be generated.

### hasController
Defaults to `true` when not given
>java -jar modgen.jar name Parkmeter hasController false

If `false` no controller will be generated.